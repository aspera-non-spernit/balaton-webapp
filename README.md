# balaton
**(under development)**

Real Estate Managing powered by [rocket 0.5](https://http://rocket.rs/).


## Components

### balaton-webapp

A frontend for the browser. Runs on:

- rocket 0.5-rc
- the handlebars temmplating engine
- and bootrap 5

#### Usage

Run on the stable channel and provide an arbitrary API as the single argument. The API Key
is the key needed to use the balaton-rest service.

**Note:** (key required but currently not used)
**Note:** Currently, there is the rust version 1.55 being released. Make sure to update rust and cargo.
If running on rust <1.56 you may need to add "cargo-features = ["edition2021"]" in the first line of the Cargo.toml

```bash
$ rustup update nightly
$ rustup set override nightly
$ cargo update
$ ROCKET_ENV=development
$ cargo run -- AN_ARBITRARY_API_KEY
```

### balaton-rest

Asynchronous middle tier. Serves requests from the balaton-webapp. Runs on a rocket.

### balaton-models

Contains all models used in balaton. Most models are (de-)serialiable and may be
re-used for similar (ie. venue booking) rust projects.

#### balaton-backend

In this development stage all models are serialized to the file system. The goal
is to provide a backend agnostic balaton-rest service.

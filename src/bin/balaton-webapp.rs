#![feature(proc_macro_hygiene, decl_macro)]
extern crate handlebars;
#[macro_use] extern crate rocket;
extern crate rocket_dyn_templates;

use balaton_models::{ io::BalatonRequest };
use handlebars::Handlebars;
use rocket::{ fs::FileServer, serde::json::* };
// use rocket_contrib::{  serve::StaticFiles, templates::Template };
use rocket_dyn_templates::Template;
use std::collections::HashMap;

/* http://localhost:8000/ */

// #[get("/administration/uebersicht")]
// fn dashboard(cookies: Cookies) -> Template {
//     let context = "";
//     match cookies.get("auth") {
//         Some(auth) => {
//             // TODO: validate
//             Template::render("dashboard", &context)
//         },
//         None => { println!("no auth"); Template::render("login", &context)}
//     }
// }

#[get("/properties")]
async fn properties() -> Template {
    let client = reqwest::Client::new();
    let req = BalatonRequest { path: "properties", query: "all" };
    let mut ctx = HashMap::<&str, Value>::new();
    match client.post("http://127.0.0.1:8001/api").json(&req).send().await {
        Ok(resp) => {
            let v: Value = serde_json::from_str(&resp.text().await.unwrap() ).unwrap();
            dbg!(&v);
            ctx.insert("properties", v);
        },
        Err(e) => { dbg!(e); }
    };
    Template::render("properties", ctx)
}

#[get("/")]
fn index() -> Template {
    println!("Hello");
    let context = "";
    Template::render("index", &context)
}

// #[get("/login")]
// fn login() -> Template {
//     let context = "";
//     Template::render("login", &context)
// }

// #[get("/redirect/<destination>")]
// fn redirect(destination: String) -> Redirect {
//     if destination == "dashboard".to_string() {
//         Redirect::to(uri!(dashboard))
//     } else {
//         Redirect::to(uri!(index))
//     }
// }

// #[post("/auth", data = "<credentials>")]
// fn auth(credentials: Form<Credentials>, mut cookies: Cookies, state: State<String>) -> Redirect {
//     let cred = credentials.clone();
//     let br = BalatonRequest { api_key: state.inner().to_string(), load: cred, path: "auth".to_string() };
//     let rt = Runtime::new().unwrap();
//     let handle = rt.handle();
//     handle.block_on( async move {
//         let client = reqwest::Client::new();
//         match client.post("http://localhost:8001/req")
//             .json(&br).send().await {
//                 Ok(response) => { 
//                     let d = response.text().await;
//                     println!("{:#?}", d);
//                     cookies.add(Cookie::new("auth", "leihdqoidhqwo"));
//                     redirect("dashboard".to_string())
//                 }
//                 Err(_e) => {  redirect("login".to_string()) }
//             }
//         }
//     )
// }

// #[post("/logout")]
// fn logout(mut cookies: Cookies) -> Flash<Redirect> {
//     cookies.remove_private(Cookie::named("user_id"));
//     Flash::success(Redirect::to("/"), "Successfully logged out.")
// }

/// Run with: // cargo run -- nr29p2öoiucnp98422ökfewifj23rb
#[launch]
fn rocket() -> _ {
    let args: Vec<String> = std::env::args().collect(); 
    let mut handlebars = Handlebars::new();
    handlebars.set_strict_mode(true);
    rocket::build()
        // .mount("/static", StaticFiles::from(concat!(env!("CARGO_MANIFEST_DIR"), "/static")))
        .mount("/static", FileServer::from("static/"))
        .mount("/", routes![index, properties])
        .manage(args[1].clone())
        .attach(Template::fairing())
}